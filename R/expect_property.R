#' Property based test expectation
#'
#' @export
expect_property <- function(that,
                            equals = \(...) TRUE,
                            for_all,
                            strict = FALSE,
                            tests = 100) {
  if (isTRUE(strict)) {
    expect_property_(
      that = that,
      equals = equals,
      for_all = for_all,
      tests = tests
    )

    expect_property_(
      that = failed(that),
      equals = \(...) TRUE,
      for_all = for_all |> map(.fail),
      tests = tests
    )
  } else {
    expect_property_(
      that = that,
      equals = equals,
      for_all = for_all,
      tests = tests
    )
  }
}

expect_property_ <- function(that,
                             equals,
                             for_all,
                             strict,
                             tests) {
  generators <-
    for_all |> map(.generate)

  hedgehog::forall(
    generators,
    \(...) {
      generator_value_list <-
        base::list(...) |> {
          \(lst) c(
            lst[names(lst) == ".."] |> purrr::flatten(),
            lst[names(lst) != ".."]
          )
        }()

      property_first_half <-
        exec(that, !!!generator_value_list)

      property_second_half <-
        exec(equals, !!!generator_value_list)

      testthat::expect_equal(
        property_first_half,
        property_second_half
      )
    },
    tests = tests
  )
}

#' Generator wrapper
#'
#' @export
arbitrary <- function(...) {
  base::list(...)
}
