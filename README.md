
<!-- README.md is generated from README.Rmd. Please edit that file -->
<!-- badges: start -->

[![CRAN
status](https://www.r-pkg.org/badges/version/quickcheck)](https://CRAN.R-project.org/package=quickcheck)
[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)

<!-- badges: end -->

# Overview

Property based testing in R, inspired by
[Quickcheck](https://en.wikipedia.org/wiki/QuickCheck). This package
builds on the property based testing framework provided by
[`hedgehog`](https://github.com/hedgehogqa/r-hedgehog) and is designed
to seamlessly integrate with a [`testthat`](https://testthat.r-lib.org).

## Installation

You can install the development version of quickcheck from
[GitHub](https://github.com/) with:

``` r
# install.packages("remotes")
remotes::install_github("quickcheckr/quickcheck")
```

# Usage

The following example uses quickcheck to test the properties of the base
R `+` function.
[Here](https://fsharpforfunandprofit.com/posts/property-based-testing/)
is an introduction to the concept of property based testing, and an
explanation of the mathematical properties of addition can be found
[here](https://www.khanacademy.org/math/cc-sixth-grade-math/cc-6th-factors-and-multiples/properties-of-numbers/a/properties-of-addition).

### Commutative property of addition

``` r
library(testthat)
library(quickcheck)

test_that("+ is commutative", {
  
  quickcheck(
    
    property = 
      (\(x, y) x + y) %=>% 
      expect_equal %=>% 
      (\(x, y) y + x),
    
    for_all = arbitrary(
      .. = ..equal_length(
        x = ..numeric(),
        y = ..numeric()
      )
    )
    
  )
  
})
#> Test passed 😀
```

### Associative property of addition

``` r
test_that("+ is associative", {
  
  quickcheck(
    
    property = 
      (\(x, y, z) x + (y + z)) %=>%
      expect_equal %=>%
      (\(x, y, z) (x + y) + z),
    
    for_all = arbitrary(
      .. = ..equal_length(
        x = ..numeric(),
        y = ..numeric(),
        z = ..numeric()
      )
    )
    
  )
  
})
#> Test passed 🎊
```

### Identity property of addition

``` r
test_that("0 is the additive identity of +", {
  
  quickcheck(
    
    property = 
      (\(x) x + 0) %=>% 
      expect_equal %=>% 
      (\(x) x),
    
    for_all = arbitrary(
      x = ..numeric()
    )
    
  )
  
})
#> Test passed 🥇
```

# Type safety

quickcheck also introduces an opinionated way to validate type-safety. R
is a weakly typed, dynamic language so errors can only be caught at
runtime. In this context a type-safe function can be considered any
function that behaves properly with valid inputs and throws an error
with invalid inputs.

For example, a type-safe addition function may only accept integer or
double vectors of equal length and it should throw if there are any NA
values, NaNs, infinites, or if the vector has attributes. This function
can be defined in base R like this.

``` r
safe_add <- function(x, y) {
  stopifnot(
    is.numeric(x),
    is.numeric(y),
    !is.na(x), 
    !is.na(y),
    !is.nan(x), 
    !is.nan(y),
    !is.infinite(x),
    !is.infinite(y),
    is.null(attributes(x)),
    is.null(attributes(y)),
    length(x) == length(y),
    length(x) >= 1, 
    length(y) >= 1
  )
  
  x + y
}
```

Testing for type-safety is as easy as adding the argument
`type_safe = TRUE`. This will cause quickcheck to automatically generate
malformed inputs and test if the function throws an error when it
should.

``` r
test_that("add is commutative and type-safe", {
  
  quickcheck(
    type_safe = TRUE,
    
    property = 
      (\(x, y) safe_add(x, y)) %=>% 
      expect_equal %=>% 
      (\(x, y) safe_add(y, x)),
    
    for_all = arbitrary(
      .. = ..equal_length(
        x = ..numeric(),
        y = ..numeric()
      )
    )
  )
  
})
#> Test passed 🌈

test_that("+ is associative and type-safe", {
  
  quickcheck(
    type_safe = TRUE,
    
    property = 
      (\(x, y, z) safe_add(x, safe_add(y, z))) %=>% 
      expect_equal %=>% 
      (\(x, y, z) safe_add(safe_add(x, y), z)),
    
    for_all = arbitrary(
      .. = ..equal_length(
        x = ..numeric(),
        y = ..numeric(),
        z = ..numeric()
      )
    )
  )
  
})
#> Test passed 🎊

test_that("0 is the additive identity of + and type-safe", {
  
  quickcheck(
    type_safe = TRUE,
    
    property = 
      (\(x, y) safe_add(x, y)) %=>% 
      expect_equal %=>% 
      (\(x, y) x),
    
    for_all = arbitrary(
      .. = ..equal_length(
        x = ..numeric(),
        y = ..numeric_bounded(0, 0)
      )
    )
  )
  
})
#> Test passed 🎉
```

This method of testing type-safety considers any data that doesn’t match
the definition in `for_all = arbitrary( ... )` as malformed. For
example, if the definition is `arbitrary(x = ..integer())` then valid
data are non-empty integer vectors of any length, containing no NAs and
having no attributes. Invalid data are anything that doesn’t fit that
definition.

Ideally quickcheck would test for every possible input. In practice,
however, to increase efficiency quickcheck only tests the most likely
culprits. In the case of integer vectors, likely culprits include double
vectors, character vectors, logical vectors, empty integer vectors,
integer vectors with attributes, and integer vectors containing NAs.
Examples of malformed data generated by quickcheck can be seen with
`..fail()`.

``` r
..integer() |> ..length(5) |> ..fail()
#> Quickcheck generator:
#> Example:
#> [1]  TRUE FALSE FALSE FALSE FALSE
..integer() |> ..length(5) |> ..fail()
#> Quickcheck generator:
#> Example:
#>  a  a    
#>  1  1 NA
..integer() |> ..length(5) |> ..fail()
#> Quickcheck generator:
#> Example:
#> [1]         0 -15165420         0         0 -16180923
..integer() |> ..length(5) |> ..fail()
#> Quickcheck generator:
#> Example:
#> [1] -24277659  68155588         0         0 -22778029
```
